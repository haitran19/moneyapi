var express = require('express');
var router = express.Router();
var firebase = require('firebase-admin');
var serviceAccount = require('../serviceAccountKey.json');


firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://moneylovermanage.firebaseio.com"
});



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/getdata', function(req, res, next) {

  var db = firebase.database();
  var ref = db.ref("users");
  ref.once("value", function(snapshot) {
    res.send(snapshot.val());
  });
});

// router.get('/setdata', function(req, res, next) {
//   var db = firebase.database();
//   var ref = db.ref("transaction");
//   ref.push({
//     user_name: "Tom",
//     wallet_id: "-LiqsU4f-GLR4c4sK8hR",
//     category_id: "-Liqu8Dpss7UeCeuY5Mf",
//     amount_money: 50000,
//     date: "26/02/2019",
//     note: "đi chơi núi"
//   })
// });

module.exports = router;
